---

## What is VPS Hosting

A VPS (Virtual Private Server) can be used for almost everything related to hosting (Apps, websites, blogs, files, data bases, etc). You can have the VPS with a control panel like cPanel/WHM, Plesk, etc which make its management easy!

The control panel which gives a web interface (and API, scripts) to interact with the operating system, services and applications installed on the VPS (or dedicated) server.

A VPS can be used for different purposes from the server role perspective: Describes the role assigned to the server based on which services and applications run on it. These are some examples.

-Database servers
-Web servers
-DNS servers
-File servers
-Proxy servers
-VPS servers
-Game servers
-Cache servers
-All-in-one servers (Run multiple services at the same time)
-etc.

You have to take into consideration that servers can be classified into two categories from the server reliability perspective:

-Low-end servers
-High-end server

**From the resources/specs perspective:

-High memory servers (For applications requiring a huge amount of memory)
-High CPU servers (For applications requiring a huge amount of CPU processing)
-High bandwidth servers (For applications requiring a huge amount of network bandwidth)
-High disk servers (For applications requiring a huge amount of disk space and I/O)
-etc.

**Additional note: 

With the huge number of available server offers and providers, comparison is a real challenge. It requires a huge amount of time. 
So we build this [cheap VPS hosting](https://www.waikey.com/) blog to monitor latest vps promotion info!
